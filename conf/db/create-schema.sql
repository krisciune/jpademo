create sequence hibernate_sequence start with 1 increment by 1
create table Author (id bigint not null, building varchar(255), city varchar(255), country varchar(255), flat varchar(255), street varchar(255), lastname varchar(255), name varchar(255), primary key (id))
create table Book (id bigint not null, edition varchar(255), isbn varchar(13), releaseDate date, title varchar(255), author_id bigint, primary key (id))
create table BOOK_GENRE (BOOK_ID bigint not null, GENRE_ID bigint not null)
create table Genre (id bigint not null, name varchar(255), primary key (id))
create table SmsMessageJpa (id bigint not null, recipientNumber varchar(255), senderNumber varchar(255), text varchar(255), primary key (id))
alter table Book add constraint FK5gbo4o7yxefxivwuqjichc67t foreign key (author_id) references Author
alter table BOOK_GENRE add constraint FKdrhy1len85521bw3rrp0dqa8n foreign key (GENRE_ID) references Genre
alter table BOOK_GENRE add constraint FKq9piri5g5cnehkiseljv35g73 foreign key (BOOK_ID) references Book
