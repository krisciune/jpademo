package itakademija.java2015.jpa.demo1.repositories;

import java.util.List;

import itakademija.java2015.jpa.assigment1.entities.Book;

public interface BookRepository {
	/*
	 * If book.id is set then update operation is performed
	 * If book.id == null then insert operation is performed
	 */
	public void insertOrUpdate(Book book);
	public void delete(Book book);
	public void deleteById(Long bookId);
	public List<Book> findByTitleFragment(String titleFragment);
	Long countAllBooks();
	public Book findByIsbn(String isbn);
	public List<Book> findByGenre(String string);
}
