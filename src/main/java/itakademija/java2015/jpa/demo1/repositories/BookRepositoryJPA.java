package itakademija.java2015.jpa.demo1.repositories;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import itakademija.java2015.jpa.assigment1.entities.Book;
import itakademija.java2015.jpa.assigment1.entities.Book_;
import itakademija.java2015.jpa.assigment1.entities.Genre;
import itakademija.java2015.jpa.assigment1.entities.Genre_;

public class BookRepositoryJPA implements BookRepository {

	private EntityManager em;

	public BookRepositoryJPA(EntityManager em) {
		this.em = em;
	}

	@Override
	public void insertOrUpdate(Book book) {
		em.getTransaction().begin();
		em.persist(book);
		em.getTransaction().commit();
	}

	@Override
	public void delete(Book book) {
		em.getTransaction().begin();
		em.remove(book);
		em.getTransaction().commit();
	}

	@Override
	public void deleteById(Long bookId) {
		em.getTransaction().begin();
		Book book = em.find(Book.class, bookId);
		if (book != null)
			em.remove(book);
		em.getTransaction().commit();
	}

	@Override
	public List<Book> findByTitleFragment(String titleFragment) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Book> cq = cb.createQuery(Book.class);
		Root<Book> bookRoot = cq.from(Book.class);
		cq.select(bookRoot); // we select entity here
		/**
		 * Construct SQL in typesafe way should result in
		 * "SELECT ... FROM book WHERE lower(title) like lower('%blahblah%')"
		 */
		cq.where(cb.like(cb.lower(bookRoot.get(Book_.title)), "%" + titleFragment.toLowerCase() + "%"));
		TypedQuery<Book> q = em.createQuery(cq);
		return q.getResultList();
	}

	@Override
	public Long countAllBooks() {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Long> countQuery = cb.createQuery(Long.class);

		countQuery.select(cb.count(countQuery.from(Book.class)));
		TypedQuery<Long> q = em.createQuery(countQuery);

		return q.getSingleResult();
	}

	@Override
	public Book findByIsbn(String isbn) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Book> cq = cb.createQuery(Book.class);
		Root<Book> bookRoot = cq.from(Book.class);
		cq.select(bookRoot); // we select entity here
		/**
		 * Construct SQL in typesafe way should result in
		 * "SELECT ... FROM book WHERE lower(title) like lower('%blahblah%')"
		 */
		cq.where(cb.equal(bookRoot.get(Book_.isbn), isbn));
		TypedQuery<Book> q = em.createQuery(cq);
		List<Book> book = q.getResultList();
		return book.get(0);
	}

	@Override
	public List<Book> findByGenre(String genreName) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Book> cq = cb.createQuery(Book.class);
		Root<Book> bookRoot = cq.from(Book.class);
		cq.select(bookRoot); // we select another entity here
		
		CriteriaQuery<Genre> cqG = cb.createQuery(Genre.class);
		Root<Genre> genreRoot = cqG.from(Genre.class);
		cqG.select(genreRoot); // we select another entity here
		cqG.where(cb.equal(genreRoot.get(Genre_.name), genreName));
		TypedQuery<Genre> qG = em.createQuery(cqG);
		List<Genre> genres = qG.getResultList();
		if (genres.size()<1) return null;
		Genre genre = genres.get(0);
		/**
		 * Construct SQL in typesafe way should result in
		 * "SELECT ... FROM book WHERE lower(title) like lower('%blahblah%')"
		 */
		cq.where(cb.isMember(genre, bookRoot.get(Book_.genres)));
		TypedQuery<Book> q = em.createQuery(cq);
		List<Book> books = q.getResultList();
		return books;
		
	}

}
