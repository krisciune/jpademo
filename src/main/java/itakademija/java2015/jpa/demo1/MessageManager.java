package itakademija.java2015.jpa.demo1;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import itakademija.java2015.jpa.demo1.entities.SmsMessageJpa;

public class MessageManager {
	static final Logger log = LoggerFactory.getLogger(Demo1.class);

	public void test() {
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("smsMessageDb");
		try {
			EntityManager entityManager = factory.createEntityManager();
			try {
				loadFromEb(entityManager);
				/** simulate sms message comming from modem **/
				SmsMessageJpa msg = recieveNextMessage();

				saveToDb(entityManager, msg);

			} finally {
				if (entityManager.isOpen())
					entityManager.close();
			}
		} finally {
			if (factory.isOpen())
				factory.close();
		}
	}

	private void loadFromEb(EntityManager entityManager) {
		/** Select currently persisted sms messages **/
		@SuppressWarnings("unchecked")
		List<SmsMessageJpa> smsList = entityManager.createQuery("SELECT e FROM SmsMessageJpa e")
				.getResultList();

		log.info("Current messages:");
		for (SmsMessageJpa sms : smsList) {
			log.info("sms: {}", sms);
		}
	}

	private void saveToDb(EntityManager entityManager, SmsMessageJpa msg) {
		/** persist sms message (save to db) **/
		entityManager.getTransaction().begin();
		entityManager.persist(msg);
		entityManager.getTransaction().commit();
	}

	private static SmsMessageJpa recieveNextMessage() {

		SmsMessageJpa msg = new SmsMessageJpa();
		msg.setSenderNumber("990238423984");
		msg.setRecipientNumber("242302342309");
		msg.setText("labas rytas");
		return msg;
	}

	public static void main(String args[]) {
		new MessageManager().test();
	}
}