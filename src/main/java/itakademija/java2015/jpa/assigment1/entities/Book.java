package itakademija.java2015.jpa.assigment1.entities;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

@Entity
public class Book {
	@Id
	@GeneratedValue
	private Long id;
	@NotNull
	private String title;
	@Temporal(TemporalType.DATE)
	private Date releaseDate;
	/**
	 * Edition of the book; For example: "second", "2nd", "anniversary", ..
	 */
	private String edition;
	@JoinColumn(name = "author_id")
	@ManyToOne(optional = true, cascade = { CascadeType.ALL })
	private Author author;
	@Column(name = "isbn", length = 13)
	private String isbn;

	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name = "BOOK_GENRE", joinColumns = {
			@JoinColumn(name = "BOOK_ID", referencedColumnName = "ID") }, inverseJoinColumns = {
					@JoinColumn(name = "GENRE_ID", referencedColumnName = "ID") })
	private Collection<Genre> genres;

	public void addGenre(Genre genre) {
		genres.add(genre);
	}
	
	public void removeGenre(Genre genre) {
		if (genres.contains(genre)) {
			genres.remove(genre);
		}
	}
	
	public Collection<Genre> getGenres() {
		return genres;
	}

	public void setGenres(Collection<Genre> genres) {
		this.genres = genres;
	}

	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Date getReleaseDate() {
		return releaseDate;
	}

	public void setReleaseDate(Date releaseDate) {
		this.releaseDate = releaseDate;
	}

	public String getEdition() {
		return edition;
	}

	public void setEdition(String edition) {
		this.edition = edition;
	}

	public Author getAuthor() {
		return author;
	}

	public void setAuthor(Author author) {
		this.author = author;
		author.addBook(this);
	}

	@Override
	public String toString() {
		return "Book [title=" + title + ", releaseDate=" + releaseDate + ", edition=" + edition + ", author=" + author
				+ "]";
	}
}
