package itakademija.java2015.jpa.assigment1.entities;

import java.util.Collection;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.validation.constraints.NotNull;

@Entity
public class Genre {
	@Id
	@GeneratedValue
	private Long id;
	@NotNull
	private String name;
	@ManyToMany(mappedBy="genres")
	private Collection<Book> books;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
		
}
