package itakademija.java2015.jpa.demo2;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.hasItems;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertFalse;
import static org.junit.Assume.assumeThat;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.RollbackException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaDelete;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import itakademija.java2015.jpa.assigment1.entities.Author;
import itakademija.java2015.jpa.assigment1.entities.Book;
import itakademija.java2015.jpa.demo1.repositories.AuthorRepositoryJPA;
import itakademija.java2015.jpa.demo1.repositories.BookRepository;
import itakademija.java2015.jpa.demo1.repositories.BookRepositoryJPA;

public class BookReviewAppTest {

	static final Logger log = LoggerFactory.getLogger(BookReviewAppTest.class);

	private static EntityManagerFactory factory;
	private EntityManager entityManager;
	private BookRepository bookRepo;

	private AuthorRepositoryJPA authorRepo;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		factory = Persistence.createEntityManagerFactory("bookReviewDb");
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		
		if (factory.isOpen())
			factory.close();
	}

	@Before
	public void setUp() throws Exception {
		entityManager = factory.createEntityManager();
		bookRepo = new BookRepositoryJPA(entityManager);
		authorRepo = new AuthorRepositoryJPA(entityManager);
		deleteAllBooks(entityManager);
		deleteAllAuthors(entityManager);
	}

	private void deleteAllBooks(EntityManager em) {
		log.debug("Deleting all books from DB!");
		try {
			em.getTransaction().begin();
			CriteriaBuilder cb = em.getCriteriaBuilder();
			
		    CriteriaDelete<Book> deleteQuery = cb.createCriteriaDelete(Book.class);
		    deleteQuery.from(Book.class);
			Query q = em.createQuery(deleteQuery);

			q.executeUpdate();

		    em.getTransaction().commit();
		} catch (SecurityException | IllegalStateException | RollbackException e) {
		    e.printStackTrace();
		}
	}
	private void deleteAllAuthors(EntityManager em) {
		log.debug("Deleting all authors from DB!");
		try {
			em.getTransaction().begin();
			CriteriaBuilder cb = em.getCriteriaBuilder();
			
		    CriteriaDelete<Author> deleteQuery = cb.createCriteriaDelete(Author.class);
		    deleteQuery.from(Author.class);
			Query q = em.createQuery(deleteQuery);

			q.executeUpdate();

		    em.getTransaction().commit();
		} catch (SecurityException | IllegalStateException | RollbackException e) {
		    e.printStackTrace();
		}
	}

	@After
	public void tearDown() throws Exception {
		if (entityManager.isOpen())
			entityManager.close();
	}

	/**
	 * Test that book repository can find books by case-insensetive search string 
	 */
	@Test
	public void testCaseInsensetiveSearchByBookTitle() {
		/**
		 * GIVEN 
		 * book db is empty
		 */
		assumeThat(bookRepo.countAllBooks(), is(equalTo(0l)));
		/**
		 * And two new books with word "Java" in their titles
		 */
		Book book1 = BookDbDataBuildHelper.buildSampleBook1();
		Book book2 = BookDbDataBuildHelper.buildSampleBook2();
		
		/*
		 * WHEN 
		 * those books are saved to DB and search performed using uppercase-only string "JAVA" 
		 */
		bookRepo.insertOrUpdate(book1);
		bookRepo.insertOrUpdate(book2);

		List<Book> books = bookRepo.findByTitleFragment("JAVA");
		
		/*
		 * THEN the same two books are returned by book repository from DB.
		 */
		assertThat(books, hasItems(book1, book2));
	}

	@Test
	public void testFindByIsbn() {
		/**
		 * GIVEN 
		 * book db is empty
		 */
		assumeThat(bookRepo.countAllBooks(), is(equalTo(0l)));
		/**
		 * And two new books with isbn "123" and "789"
		 */
		Book book1 = BookDbDataBuildHelper.buildSampleBook1();
		Book book2 = BookDbDataBuildHelper.buildSampleBook2();
		
		/*
		 * WHEN 
		 * those books are saved to DB and search performed using strings "123" and "789" 
		 */
		bookRepo.insertOrUpdate(book1);
		bookRepo.insertOrUpdate(book2);

		List<Book> books = new ArrayList<Book>();
		books.add(bookRepo.findByIsbn("123"));
		books.add(bookRepo.findByIsbn("789"));
		
		/*
		 * THEN the same two books are returned by book repository from DB.
		 */
		assertThat(books, hasItems(book1, book2));
	}

	
	@Test
	public void testGenreCanBeAdded() {
		/**
		 * GIVEN 
		 * book db is empty
		 */
		assumeThat(bookRepo.countAllBooks(), is(equalTo(0l)));
		/**
		 * Create two new books with genre "thriller" and one with genre "math"
		 */
		Book book1 = BookDbDataBuildHelper.buildSampleBook1();
		Book book2 = BookDbDataBuildHelper.buildSampleBook1();
		Book book3 = BookDbDataBuildHelper.buildSampleBook2();
		
		/*
		 * WHEN 
		 * those books are saved to DB and search performed using strings "123" and "789" 
		 */
		bookRepo.insertOrUpdate(book1);
		bookRepo.insertOrUpdate(book2);
		bookRepo.insertOrUpdate(book3);

		List<Book> books = bookRepo.findByGenre("thriller");
		
		/*
		 * THEN the same two books are returned by book repository from DB.
		 */
		assertThat(books, hasItems(book2, book3));
	}

	
	
	
	/**
	 * Test, that after saving graph Book<->Author,
	 * when the book is found, author is reachable by book class's accessor method
	 */
	@Test
	public void testThatAuthorIsRetrievedWithBook() {
		/**
		 * GIVEN 
		 * We build two objects: book and author and reference author from book 
		 */

		Book book1 = BookDbDataBuildHelper.buildSampleBook1();
	
		/*
		 * WHEN 
		 * book is saved to DB and then is searched 
		 */
		bookRepo.insertOrUpdate(book1);

		List<Book> books = bookRepo.findByTitleFragment(book1.getTitle());
		
		/*
		 * THEN 
		 * retrieved book should have a reference to author, as in original graph 
		 */
		assertFalse(books.isEmpty());
		Book foundBook = books.stream().findFirst().get();
		assertThat(foundBook.getAuthor(), is(notNullValue()));
	}
	
	@Test
	public void testBidirectionalAuthorBookAssociasion() {
		/**
		 * GIVEN 
		 * We build two objects: book and author and reference author from book 
		 */

		Book book1 = BookDbDataBuildHelper.buildSampleBook1();
	
		/*
		 * WHEN 
		 * book is saved to DB and then books author is searched 
		 */
		bookRepo.insertOrUpdate(book1);
		
		String searchedName = book1.getAuthor().getName();
		log.debug("Searching author by name '{}'", searchedName);
		Author authorFromDb = authorRepo.findFirstByName(searchedName);

		log.info("Found author: {}", authorFromDb);
		
		/*
		 * THEN 
		 * retrieved author should have back-reference to book  
		 */
		List<Book> books = authorFromDb.getBooks();
		assertFalse(books.isEmpty());
		Book foundBook = books.stream().findFirst().get();
		assertThat(foundBook.getId(), is(equalTo(book1.getId())));
	}


}
